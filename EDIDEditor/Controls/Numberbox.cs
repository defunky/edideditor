﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EDIDEditor.Controls
{
    [TemplatePart(Name = "PART_Label", Type = typeof(System.Windows.Controls.Label))]
    [TemplatePart(Name = "PART_Value", Type = typeof(System.Windows.Controls.TextBox))]
    public class Numberbox : TextBox
    {
        static Numberbox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Numberbox), new FrameworkPropertyMetadata(typeof(Numberbox)));
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            string num = e.Key.ToString().Substring(e.Key.ToString().Length - 1);
            if (!int.TryParse(num, out _))
                e.Handled = true;

            base.OnKeyDown(e);
        }

        public string Label
        {
            get => (string)GetValue(LabelProperty);
            set => SetValue(LabelProperty, value);
        }
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(Numberbox));

        public double TextBoxWidth
        {
            get => (double)GetValue(TextBoxWidthProperty);
            set => SetValue(TextBoxWidthProperty, value);
        }

        public static readonly DependencyProperty TextBoxWidthProperty =
            DependencyProperty.Register("TextBoxWidth", typeof(double), typeof(Numberbox));
    }
}
