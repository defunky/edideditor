﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EDIDEditor.Helpers
{
    public class RelayCommand : ICommand
    {
        private readonly Action m_execute;
        private readonly Func<bool> m_canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            m_execute = execute ?? throw new ArgumentNullException("execute");

            if (canExecute != null)
            {
                m_canExecute = canExecute;
            }
        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute();
        }

        public void Execute(object parameter)
        {
            if (m_execute != null && CanExecute(parameter))
            {
               m_execute();
            }
        }

        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        /// <summary>
        /// Taken from MVVM Light RelayCommand
        /// </summary>
        private EventHandler m_requerySuggestedLocal;
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (m_canExecute != null)
                {
                    // add event handler to local handler backing field in a thread safe manner
                    EventHandler handler2;
                    EventHandler canExecuteChanged = m_requerySuggestedLocal;

                    do
                    {
                        handler2 = canExecuteChanged;
                        EventHandler handler3 = (EventHandler)Delegate.Combine(handler2, value);
                        canExecuteChanged = System.Threading.Interlocked.CompareExchange(
                            ref m_requerySuggestedLocal,
                            handler3,
                            handler2);
                    }
                    while (canExecuteChanged != handler2);

                    CommandManager.RequerySuggested += value;
                }
            }

            remove
            {
                if (m_canExecute != null)
                {
                    // removes an event handler from local backing field in a thread safe manner
                    EventHandler handler2;
                    EventHandler canExecuteChanged = m_requerySuggestedLocal;

                    do
                    {
                        handler2 = canExecuteChanged;
                        EventHandler handler3 = (EventHandler)Delegate.Remove(handler2, value);
                        canExecuteChanged = System.Threading.Interlocked.CompareExchange(
                            ref m_requerySuggestedLocal,
                            handler3,
                            handler2);
                    }
                    while (canExecuteChanged != handler2);

                    CommandManager.RequerySuggested -= value;
                }
            }
        }
    }
}
