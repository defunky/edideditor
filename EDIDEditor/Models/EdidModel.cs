﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using EDIDParser.Descriptors;
using JetBrains.Annotations;

namespace EDIDEditor.Models
{
    public class EdidModel : IEdidModel
    {
        private byte[] m_edidBytes;
        private IList<IDescriptor> m_descriptors;
        private DescriptorFactory m_factory;

        public EdidModel()
        {
            m_factory = new DescriptorFactory();
        }

        public void Load(string filePath)
        {
            m_edidBytes = m_factory.readEDIDFile(filePath);
            Descriptors = m_factory.CreateDescriptors();
        }

        public void Save(string filePath)
        {
            m_factory.saveEDIDFile(filePath);
        }

        public IList<IDescriptor> Descriptors
        {
            get => m_descriptors;
            set
            {
                m_descriptors = value;
                RaisePropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
