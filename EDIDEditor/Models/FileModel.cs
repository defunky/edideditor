﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace EDIDEditor.Models
{
    public class FileModel : IFileModel
    {
        private readonly IEdidModel m_edidModel;

        public FileModel(IEdidModel edidModel)
        {
            m_edidModel = edidModel;
        }

        public void LoadFile()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Binary Files (*.bin)|*.bin|All files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                m_edidModel.Load(openFileDialog.FileName);
            }
		}

        public void SaveFile()
        {

            var dialog = new SaveFileDialog()
            {
                Filter = "Binary File(*.bin)|*.bin|All(*.*)|*"
            };

            if (dialog.ShowDialog() == true)
            {
                m_edidModel.Save(dialog.FileName);
            }
        }
    }
}
