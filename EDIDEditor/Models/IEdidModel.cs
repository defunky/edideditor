﻿using System.Collections.Generic;
using System.ComponentModel;
using EDIDParser.Descriptors;

namespace EDIDEditor.Models
{
    public interface IEdidModel : INotifyPropertyChanged
    {
        void Load(string filePath);
        void Save(string filePath);
        IList<IDescriptor> Descriptors { get; set; }
    }
}