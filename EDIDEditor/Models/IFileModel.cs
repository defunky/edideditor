﻿namespace EDIDEditor.Models
{
    public interface IFileModel
    {
        void LoadFile();
        void SaveFile();
    }
}