﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using EDIDEditor.Models;
using EDIDParser.Descriptors;
using JetBrains.Annotations;

namespace EDIDEditor.ViewModels
{
    public class DetailedBlockViewModel : INotifyPropertyChanged
    {
        private DescriptorType m_selectedDescriptor;
        private DescriptorFactory m_factory;

        public DetailedBlockViewModel(IDescriptor descriptor)
        {
            DescriptorTypes = new ObservableCollection<DescriptorType>(Enum.GetValues(typeof(DescriptorType)).Cast<DescriptorType>());
            m_factory = new DescriptorFactory();
            m_selectedDescriptor = descriptor.Type;
            Descriptor = descriptor;
        }

        public ObservableCollection<DescriptorType> DescriptorTypes { get; set; }

        public DescriptorType SelectedDescriptor
        {
            get => m_selectedDescriptor;
            set
            {
                m_selectedDescriptor = value;
                Descriptor = m_factory.CreateDescriptor(m_selectedDescriptor, new byte[18]);
                RaisePropertyChanged(nameof(Descriptor));
            }
        }

        public IDescriptor Descriptor { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

