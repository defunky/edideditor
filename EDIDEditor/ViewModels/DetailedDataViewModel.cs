﻿using EDIDParser.Descriptors;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using EDIDEditor.Models;

namespace EDIDEditor.ViewModels
{
    public class DetailedDataViewModel
    {
        private readonly IEdidModel m_edidModel;

        public DetailedDataViewModel(IEdidModel edidModel)
        {
            m_edidModel = edidModel;
            generateDescriptorViewModels();
            edidModel.PropertyChanged += EdidModel_PropertyChanged;
        }

        private void EdidModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(m_edidModel.Descriptors))
            {
                //Descriptors were updated so refresh the viewmodels
                generateDescriptorViewModels();
            }
        }

        private void generateDescriptorViewModels()
        {
            for (int i = 0; i < 4; i++)
            {
                var descriptor = m_edidModel.Descriptors[i];
                if (DetailedBlockViewModels.Count <= i)
                {
                    DetailedBlockViewModels.Add(new DetailedBlockViewModel(descriptor));
                }
                else
                {
                    DetailedBlockViewModels[i] = new DetailedBlockViewModel(descriptor);
                }
            }
        }

        public IList<DetailedBlockViewModel> DetailedBlockViewModels { get; set; } =
            new ObservableCollection<DetailedBlockViewModel>();
    }

}
