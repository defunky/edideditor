﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using EDIDEditor.Helpers;
using EDIDEditor.Models;
using EDIDParser.Descriptors;

namespace EDIDEditor.ViewModels
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel(IEdidModel edidModel, IFileModel fileModel)
        {
            edidModel.Load(@"..\..\..\..\mysave1.bin");
            LoadFile = new RelayCommand(fileModel.LoadFile);
            SaveFile = new RelayCommand(fileModel.SaveFile);
        }

        public ICommand LoadFile { get; set; }
        public ICommand SaveFile { get; set; }
    }
}
