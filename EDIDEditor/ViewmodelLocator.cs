﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EDIDEditor.Models;
using EDIDEditor.ViewModels;
using EDIDEditor.Views;

namespace EDIDEditor
{
    public class ViewModelLocator
    {
        private IContainer m_container;

        public ViewModelLocator()
        {
            var builder = new ContainerBuilder();

            //Register view models
            builder.RegisterAssemblyTypes(Assembly.Load(nameof(EDIDEditor)))
                .Where(x => x.Namespace.Contains("ViewModels"));

            //Register models
            builder.RegisterAssemblyTypes(Assembly.Load(nameof(EDIDEditor))).
                Where(x => x.Namespace.Contains("Models") && x.Name != "DescriptorModel")
                .AsImplementedInterfaces();

            //Register a singleton for edid model as we want same data access in all views
            builder.RegisterType<EdidModel>().AsImplementedInterfaces().SingleInstance();

            m_container = builder.Build();
        }

        public MainWindowViewModel MainWindowViewModel => m_container.Resolve<MainWindowViewModel>();
        public DetailedDataViewModel DetailedDataViewModel => m_container.Resolve<DetailedDataViewModel>();
        //public DetailedBlockViewModel DetailedBlockViewModel => m_container.Resolve<DetailedBlockViewModel>();

    }
}
