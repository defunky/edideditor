﻿<UserControl x:Class="EDIDEditor.Views.DescriptorViews.DetailedTimingView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:local="clr-namespace:EDIDEditor.Views.DescriptorViews"
             xmlns:controls="clr-namespace:EDIDEditor.Controls"
             xmlns:descriptors="clr-namespace:EDIDParser.Descriptors;assembly=EDIDParser"
             xmlns:converters="clr-namespace:EDIDEditor.Converters"
             mc:Ignorable="d" 
             d:DesignHeight="450" d:DesignWidth="800"
             d:DataContext="{d:DesignInstance Type=descriptors:DetailedTimingDescriptor,
                IsDesignTimeCreatable=True}">
    <UserControl.Resources>
        <converters:EnumDescriptionConverter x:Key="enumConverter"/>
    </UserControl.Resources>
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
        </Grid.RowDefinitions>

        <controls:Numberbox Grid.Row="0" Grid.Column="0" Label="Pixel Clock:" Text="{Binding PixelClock}"/>
        <controls:Numberbox Grid.Row="1" Grid.Column="0" Label="X Active Pixels:" Text="{Binding HorizontalActivePixels}"/>
        <controls:Numberbox Grid.Row="2" Grid.Column="0" Label="X Blank:" Text="{Binding HorizontalBlank}"/>
        <controls:Numberbox Grid.Row="3" Grid.Column="0" Label="X Front Porch:" Text="{Binding HorizontalFrontPorch}"/>
        <controls:Numberbox Grid.Row="4" Grid.Column="0" Label="X Sync Width:" Text="{Binding HorizontalSyncWidth}"/>
        <controls:Numberbox Grid.Row="5" Grid.Column="0" Label="X Image Size:" Text="{Binding HorizontalImageSize}"/>
        <controls:Numberbox Grid.Row="6" Grid.Column="0" Label="X Border:" Text="{Binding HorizontalBorder}"/>

        <Grid Grid.Row="0" Grid.Column="1">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="Auto"/>
            </Grid.ColumnDefinitions>
            <Label VerticalAlignment="Center" Grid.Column="0" Content="Interlace:"/>
            <CheckBox VerticalAlignment="Center" Grid.Column="1" IsChecked="{Binding Interlaced}"/>
        </Grid>

        <controls:Numberbox Grid.Row="1" Grid.Column="1" Label="Y Active Pixels:" Text="{Binding VerticalActiveLines}"/>
        <controls:Numberbox Grid.Row="2" Grid.Column="1" Label="Y Blank:" Text="{Binding VerticalBlank}"/>
        <controls:Numberbox Grid.Row="3" Grid.Column="1" Label="Y Front Porch:" Text="{Binding VerticalFrontPorch}"/>
        <controls:Numberbox Grid.Row="4" Grid.Column="1" Label="Y Sync Width:" Text="{Binding VerticalSyncWidth}"/>
        <controls:Numberbox Grid.Row="5" Grid.Column="1" Label="Y Image Size:" Text="{Binding VerticalImageSize}"/>
        <controls:Numberbox Grid.Row="6" Grid.Column="1" Label="Y Border:" Text="{Binding VerticalBorder}"/>

        <Label Grid.Row="7" Grid.Column="0" Grid.ColumnSpan="2" Content="Stereo Mode:"/>
        <ComboBox Grid.Row="8"  Grid.Column="0" Grid.ColumnSpan="2" ItemsSource="{Binding StereoModes}" SelectedItem="{Binding StereoMode}" Height="25">
            <ComboBox.ItemTemplate>
                <DataTemplate>
                    <TextBlock Text="{Binding Converter={StaticResource enumConverter}}"/>
                </DataTemplate>
            </ComboBox.ItemTemplate>
        </ComboBox>

        <Label Grid.Row="9" Grid.Column="0" Grid.ColumnSpan="2" Content="Signal Sync:"/>
        <ComboBox Grid.Row="10"  Grid.Column="0" Grid.ColumnSpan="2" ItemsSource="{Binding SyncSignals}" SelectedItem="{Binding SyncSignal}" Height="25">
            <ComboBox.ItemTemplate>
                <DataTemplate>
                    <TextBlock Text="{Binding Converter={StaticResource enumConverter}}"/>
                </DataTemplate>
            </ComboBox.ItemTemplate>
        </ComboBox>

        <StackPanel Grid.Row="11" Grid.Column="0" Grid.ColumnSpan="2" Orientation="Horizontal">
            <Label Content="Vsync Polarity:">
                <Label.Style>
                    <Style TargetType="{x:Type Label}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.Analog}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.BipolarAnalog}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.DigitalComposite}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Label.Style>
            </Label>
            <Label Content="Serration:">
                <Label.Style>
                    <Style TargetType="{x:Type Label}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.DigitalSeparate}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Label.Style>
            </Label>
            <CheckBox VerticalAlignment="Center" IsChecked="{Binding VSyncPolarityOrSerration}"/>
            <Label Content="Hsync Polarity:">
                <Label.Style>
                    <Style TargetType="{x:Type Label}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.Analog}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.BipolarAnalog}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Label.Style>
            </Label>
            <Label Content="Sync RGB:">
                <Label.Style>
                    <Style TargetType="{x:Type Label}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.DigitalComposite}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding SyncSignal}" Value="{x:Static descriptors:SyncSignal.DigitalSeparate}">
                                <Setter Property="Visibility" Value="Collapsed"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Label.Style>
            </Label>
            <CheckBox VerticalAlignment="Center" IsChecked="{Binding HSyncPolarityOrRgb}"/>
        </StackPanel>

    </Grid>
</UserControl>
