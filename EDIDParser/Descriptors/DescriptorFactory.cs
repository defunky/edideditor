﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDIDParser.Descriptors
{
    public class DescriptorFactory
    {
        //Edid can only contain 4 descriptors
        private const int DESCRIPTOR_AMOUNT = 4;
        //A descriptor is 18 bytes big.
        private const int DESCRIPTOR_SIZE = 18;
        //The first descriptor starts at byte 54
        private const int DESCRIPTOR_OFFSET = 54;
        private IList<IDescriptor> m_descriptors;
        private byte[] m_edidBytes;


        public byte[] readEDIDFile(string fileName)
        {
            using (var reader = new BinaryReader(File.OpenRead(fileName)))
            {
                //Read the bytes from the edid bin file
                m_edidBytes = reader.ReadBytes((int)new FileInfo(fileName).Length);
            }

            return m_edidBytes;
        }

        public void saveEDIDFile(string fileName)
        {
            using var writer = new BinaryWriter(File.OpenWrite(fileName));
            //Write bytes to an edid bin file
            for (int i = 0; i < DESCRIPTOR_AMOUNT; i++)
            {
                var descriptorBytes = m_descriptors[i].DescriptorBytes;

                Array.Copy(descriptorBytes, 0,
                    m_edidBytes, DESCRIPTOR_OFFSET + DESCRIPTOR_SIZE * i, DESCRIPTOR_SIZE);
            }

            //Write the new checksum
            m_edidBytes[127] = (byte) calculateChecksum();

            writer.Write(m_edidBytes);
        }

        private int calculateChecksum()
        {
            var sum = 0;
            //Sum up all of base EDID Data
            for (int i = 0; i < 127; i++)
            {
                sum += m_edidBytes[i];
            }

            //Sum of all 128 bytes should equal 0 (mod 256).
            return 256 - (sum % 256);
        }

        public IList<IDescriptor> CreateDescriptors()
        {
            m_descriptors = new List<IDescriptor>();

            for (int i = 0; i < DESCRIPTOR_AMOUNT; i++)
            {
                var descriptorBytes = new byte[DESCRIPTOR_SIZE];

                Array.Copy(m_edidBytes,
                    DESCRIPTOR_OFFSET + DESCRIPTOR_SIZE * i,
                    descriptorBytes, 0, DESCRIPTOR_SIZE);

                //For non-detailed timing descriptor, first byte indicates it's a display descriptor
                //2nd byte should always be set to 0 on non-detailed descriptors
                if (descriptorBytes[0] != 0 && descriptorBytes[1] != 0)
                {
                    var detailedDesc = createDetailedTiming(descriptorBytes);
                    m_descriptors.Add(detailedDesc);
                }
                else
                {
                    DescriptorType descriptorType = (DescriptorType)descriptorBytes[3];
                    var descriptor = CreateDescriptor(descriptorType, descriptorBytes);
                    m_descriptors.Add(descriptor);
                }

            }

            return m_descriptors;
        }

        //public IDescriptor CreateDescriptor(DescriptorType type)
        //{
        //    switch (type)
        //    {
        //        case DescriptorType.Unused:
        //            return new UnusedDescriptor(null);
        //        case DescriptorType.DetailedTiming:
        //            return new DetailedTimingDescriptor();
        //        default:
        //            throw new ArgumentOutOfRangeException(nameof(type), type, null);
        //    }
        //}

        private IDescriptor createDetailedTiming(byte[] descriptorBytes)
        {
            return new DetailedTimingDescriptor(descriptorBytes);
        }

        public IDescriptor CreateDescriptor(DescriptorType descriptorType, byte[] descriptorBytes)
        {
            switch (descriptorType)
            {
                case DescriptorType.DisplaySerialNumber:
                case DescriptorType.UnspecifiedText:
                case DescriptorType.DisplayRangeLimits:
                case DescriptorType.DisplayName:
                case DescriptorType.AdditionalWhitePointData:
                case DescriptorType.AdditionalStandardTimings:
                case DescriptorType.DisplayColorManagement:
                case DescriptorType.CVT:
                case DescriptorType.AdditionalStandardTiming3:
                case DescriptorType.DummyIdentifier:
                default:
                   return new UnusedDescriptor(descriptorBytes);
            }
        }
    }
}
