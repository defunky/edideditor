﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDIDParser.Descriptors
{
    public enum DescriptorType
    {
        [Description("Unused")]
        Unused = -1,
        [Description("Detailed Timing")]
        DetailedTiming = 0x2,
        [Description("Display serial number (ASCII text)")]
        DisplaySerialNumber = 0xFF,
        [Description("Unspecified text (ASCII text)")]
        UnspecifiedText = 0xFE,
        [Description("Display range limits")]
        DisplayRangeLimits = 0xFD,
        [Description("Display name (ASCII text)")]
        DisplayName = 0xFC,
        [Description("Additional white point data")]
        AdditionalWhitePointData = 0xFB,
        [Description("Additional standard timing identifiers")]
        AdditionalStandardTimings = 0xFA,
        [Description("Display Color Management (DCM)")]
        DisplayColorManagement = 0xF9,
        [Description("CVT 3-Byte Timing Codes")]
        CVT = 0xF8,
        [Description("Additional standard timing 3")]
        AdditionalStandardTiming3 = 0xF7,
        [Description("Dummy identifier")]
        DummyIdentifier = 0x10,

        //00–0F: Manufacturer reserved descriptors.
    }
}
