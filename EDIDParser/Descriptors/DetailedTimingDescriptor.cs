﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDIDParser.Descriptors
{
    public class DetailedTimingDescriptor : IDescriptor 
    {
        public byte[] DescriptorBytes { get; }

        public DetailedTimingDescriptor()
        {
        }

        public DetailedTimingDescriptor(byte[] descriptorBytes)
        {
            DescriptorBytes = descriptorBytes;

            //Byte 2 defines Horizontal active pixels 8 least significant bits (0–4095)
            //Byte 4 defines Horizontal active pixels 4 most significant bits
            HorizontalActivePixels = 1920;

            //Byte 5 defines Vertical active lines 8 least significant bits (0–4095)
            //Byte 7 defines Vertical active lines 4 most significant bits
            VerticalActiveLines = descriptorBytes[5] + ((descriptorBytes[7] & 0xf0) << 4);

            HorizontalBlank = descriptorBytes[3] + ((descriptorBytes[4] & 0x0f) << 8);
            VerticalBlank = descriptorBytes[6] + ((descriptorBytes[7] & 0x0f) << 8);

            HorizontalFrontPorch = descriptorBytes[8] + (descriptorBytes[11] & 0xc0);
            VerticalFrontPorch = ((descriptorBytes[10] & 0xf0) >> 4) + ((descriptorBytes[11] & 0x0c) << 2);


            HorizontalSyncWidth = descriptorBytes[9] + ((descriptorBytes[11] & 0x30) << 2);
            VerticalSyncWidth = (descriptorBytes[10] & 0x0F) + ((descriptorBytes[11] & 0x3) << 4);

            HorizontalImageSize = descriptorBytes[12] + ((descriptorBytes[14] & 0xf0) << 4);
            VerticalImageSize = descriptorBytes[13] + ((descriptorBytes[14] & 0x0f) << 8);


            HorizontalBorder = descriptorBytes[15];
            VerticalBorder = descriptorBytes[16];

            StereoMode = (StereoMode)(descriptorBytes[17] & 0x61);
            SyncSignal = (SyncSignal) (descriptorBytes[17] & 0x18); /*--Composite has serration only. Seperate has vsync*/
            VSyncPolarityOrSerration = Convert.ToBoolean(descriptorBytes[17] & 0x04);
            HSyncPolarityOrRgb = Convert.ToBoolean(descriptorBytes[17] & 0x02);

            //Bit 7 tells us if it's interlaced
            Interlaced = Convert.ToBoolean(descriptorBytes[17] & 0x80);
            //Set a bit ^= (1 << n)
            //Clear a bit num &= (~(1 << n));
        }

        public DescriptorType Type => DescriptorType.DetailedTiming;

        //TODO: Find the decimal - in terms of 10KHz
        public float PixelClock
        {
            get => (DescriptorBytes[0] + (DescriptorBytes[1] << 8)) * 10;
            set
            {
                var valueBytes = BitConverter.GetBytes((int)value);
                DescriptorBytes[0] = valueBytes[0];
                DescriptorBytes[1] = valueBytes[1];
            }
        }

        public bool Interlaced { get; set; }

        public int HorizontalActivePixels
        {
            get => DescriptorBytes[2] + ((DescriptorBytes[4] & 0xf0) << 4);
            set
            {
                var valueBytes = BitConverter.GetBytes(value);
                DescriptorBytes[2] = valueBytes[0];
                DescriptorBytes[4] = valueBytes[1];
            }
        }

        public int HorizontalBlank { get; set; }
        public int HorizontalFrontPorch { get; set; }
        public int HorizontalSyncWidth { get; set; }
        public int HorizontalImageSize { get; set; }
        public int HorizontalBorder { get; set; }

        public int VerticalActiveLines { get; set; }
        public int VerticalBlank { get; set; }
        public int VerticalFrontPorch { get; set; }
        public int VerticalSyncWidth { get; set; }
        public int VerticalImageSize { get; set; }
        public int VerticalBorder { get; set; }

        public ObservableCollection<StereoMode> StereoModes { get; set; } =
            new ObservableCollection<StereoMode>(Enum.GetValues(typeof(StereoMode)).Cast<StereoMode>());
        public StereoMode StereoMode { get; set; }

        public ObservableCollection<SyncSignal> SyncSignals { get; set; } =
            new ObservableCollection<SyncSignal>(Enum.GetValues(typeof(SyncSignal)).Cast<SyncSignal>());
        public SyncSignal SyncSignal { get; set; }
        public bool VSyncPolarityOrSerration { get; set; }
        public bool HSyncPolarityOrRgb { get; set; }
    }
}
