﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDIDParser.Descriptors
{
    public interface IDescriptor
    {
        DescriptorType Type { get; }
        public byte[] DescriptorBytes { get; }

    }
}
