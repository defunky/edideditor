﻿using System.ComponentModel;

namespace EDIDParser.Descriptors
{
    public enum StereoMode
    {
        None = 0x0,
        [Description("Field sequential, right during stereo sync")]
        FSRight = 0x20,
        [Description("Field sequential, left during stereo sync")]
        FSLeft = 0x40,
        [Description("2-way interleaved, right image on even lines")]
        TwoWayRI = 0x21,
        [Description("2-way interleaved, left image on even lines")]
        TwoWayLI = 0x41,
        [Description("4-way interleaved")]
        FourWay = 0x60,
        [Description("Side-by-side interleaved")]
        SideBySide = 0x61
    }
}