﻿using System.ComponentModel;

namespace EDIDParser.Descriptors
{
    public enum SyncSignal
    {
        [Description("Analog Composite")]
        Analog = 0x0,
        [Description("Bipolar Analog Composite")]
        BipolarAnalog = 0x8,
        [Description("Digital Composite")]
        DigitalComposite = 0x10,
        [Description("Digital Seperate")]
        DigitalSeparate = 0x18,
    }
}