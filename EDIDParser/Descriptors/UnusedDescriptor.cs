﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDIDParser.Descriptors
{
    public class UnusedDescriptor : IDescriptor
    {
        public UnusedDescriptor(byte[] descriptorBytes)
        {
            DescriptorBytes = descriptorBytes;
        }
        public byte[] DescriptorBytes { get; }
        public DescriptorType Type => DescriptorType.Unused;
    }
}
